#[macro_use]
extern crate thiserror;

#[cfg(feature = "actix")]
pub use crate::actix_impl::*;

use chrono::{Duration, Utc};
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};
use serde::{self, Deserialize, Serialize};
use serde_json::{to_value, Value};
use std::collections::HashMap;
use std::env;

#[cfg(feature = "actix")]
mod actix_impl;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Token<T: Default> {
    pub sub: T,
    iat: i64,
    nbf: i64,
    exp: i64,
    #[serde(flatten)]
    pub extra: HashMap<String, Value>,
}

impl<T> Token<T>
where
    T: serde::Serialize + serde::de::DeserializeOwned + Default,
{
    pub fn from_header(header: &str) -> Result<Self, JwtError> {
        if header.starts_with("Bearer ") && header.len() > 7 {
            let token = &header[7..];

            let token = decode::<Token<T>>(
                token,
                &DecodingKey::from_secret(
                    env::var("SECRET_KEY")
                        .expect("SECRET_KEY not set")
                        .as_bytes(),
                ),
                &Validation::default(),
            )
            .map_err(|e| JwtError::TokenInvalid {
                cause: e.to_string(),
            })?;

            let token = token.claims;
            if token.is_valid() {
                return Ok(token);
            }
        }
        Err(JwtError::TokenInvalid {
            cause: "Invalid format".to_string(),
        })
    }

    pub fn new(user_id: T, extra: HashMap<String, Value>) -> Self {
        let current_time = Utc::now();
        Token {
            sub: user_id,
            iat: current_time.timestamp(),
            nbf: current_time.timestamp(),
            exp: (current_time + Duration::hours(1)).timestamp(),
            extra,
        }
    }

    pub fn with_duration(user_id: T, extra: HashMap<String, Value>, duration: Duration) -> Self {
        let current_time = Utc::now();
        Token {
            sub: user_id,
            iat: current_time.timestamp(),
            nbf: current_time.timestamp(),
            exp: (current_time + duration).timestamp(),
            extra,
        }
    }

    pub fn client_token(client_id: String) -> Self {
        let current_time = Utc::now();
        Token {
            sub: T::default(),
            iat: current_time.timestamp(),
            nbf: current_time.timestamp(),
            exp: (current_time + Duration::hours(1)).timestamp(),
            extra: {
                let mut map = HashMap::new();
                map.insert("client_id".to_string(), to_value(client_id).unwrap());
                map
            },
        }
    }

    pub fn encode(&self) -> String {
        encode(
            &Header::default(),
            self,
            &EncodingKey::from_secret(
                env::var("SECRET_KEY")
                    .expect("SECRET_KEY not set")
                    .as_bytes(),
            ),
        )
        .expect("Encoding a JWT should not fail")
    }

    pub fn is_valid(&self) -> bool {
        let now = Utc::now().timestamp();
        self.nbf <= now && now < self.exp
    }
}

#[derive(Debug, Error)]
pub enum JwtError {
    #[error("No Authorization header was provided")]
    HeaderMissing,
    #[error("The Bearer token provided was not valid: {cause}")]
    TokenInvalid { cause: String },
}
