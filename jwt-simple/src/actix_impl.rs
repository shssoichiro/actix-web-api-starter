use crate::JwtError;
use crate::Token;
use actix_web::http::StatusCode;
use actix_web::FromRequest;
use actix_web::ResponseError;
use actix_web::{dev, HttpRequest, HttpResponse};
use futures::future::{err, ok, Ready};
use serde::de::DeserializeOwned;
use serde::Serialize;
use std::ops::Deref;

pub struct JwtAuth<T>(Token<T>)
where
    T: Serialize + DeserializeOwned + Default;

impl<T> FromRequest for JwtAuth<T>
where
    T: Serialize + DeserializeOwned + Default,
{
    type Config = ();
    type Error = JwtError;
    type Future = Ready<Result<JwtAuth<T>, Self::Error>>;

    fn from_request(
        req: &HttpRequest,
        _payload: &mut dev::Payload,
    ) -> <Self as FromRequest>::Future {
        let header = req.headers().get("Authorization");
        if let Some(header) = header {
            return match Token::from_header(header.to_str().unwrap()) {
                Ok(token) => ok(JwtAuth(token)),
                Err(e) => err(e),
            };
        }
        err(JwtError::HeaderMissing)
    }
}

impl<T> Deref for JwtAuth<T>
where
    T: Serialize + DeserializeOwned + Default,
{
    type Target = Token<T>;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl ResponseError for JwtError {
    fn error_response(&self) -> HttpResponse {
        HttpResponse::new(StatusCode::UNAUTHORIZED)
    }
}
