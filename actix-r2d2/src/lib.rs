use actix;
use actix::prelude::*;
use anyhow::Result;
use diesel::prelude::*;
use diesel_logger::LoggingConnection as LoggingConnectionInner;
use r2d2::Pool;
use r2d2_diesel::ConnectionManager;

pub type LoggingConnection = LoggingConnectionInner<PgConnection>;

pub struct DbExecutor(pub Pool<ConnectionManager<LoggingConnection>>);

impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}

pub trait DbMessage: Message {
    fn handle(self, conn: &LoggingConnection) -> Self::Result;
}

impl<T> Handler<T> for DbExecutor
where
    T: DbMessage + Message,
    <T as Message>::Result: actix::dev::MessageResponse<DbExecutor, T>,
{
    type Result = <T as Message>::Result;

    fn handle(&mut self, msg: T, _: &mut Self::Context) -> Self::Result {
        let conn = self.0.get().unwrap();
        msg.handle(&conn)
    }
}

pub fn create_executors(db_url: &str) -> Result<Addr<DbExecutor>> {
    let pool = Pool::new(ConnectionManager::new(db_url))?;
    Ok(SyncArbiter::start(3, move || DbExecutor(pool.clone())))
}
