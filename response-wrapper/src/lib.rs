use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct StandardResponse<T> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub message: Option<Message>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub errors: Option<Errors>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub data: Option<T>,
    #[serde(flatten)]
    pub pagination: Pagination,
}

impl<T> Default for StandardResponse<T> {
    fn default() -> Self {
        StandardResponse {
            message: None,
            errors: None,
            data: None,
            pagination: Pagination::default(),
        }
    }
}

impl<T> StandardResponse<T>
where
    T: Serialize,
{
    pub fn message(mut self, message: Message) -> Self {
        self.message = Some(message);
        self
    }

    pub fn errors(mut self, errors: Errors) -> Self {
        self.errors = Some(errors);
        self
    }

    pub fn data(mut self, data: T) -> Self {
        self.data = Some(data);
        self
    }

    pub fn pagination(mut self, pagination: Pagination) -> Self {
        self.pagination = pagination;
        self
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Message {
    pub severity: MessageSeverity,
    pub text: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
#[serde(rename_all = "lowercase")]
pub enum MessageSeverity {
    Success,
    Info,
    Notice,
    Warning,
    Error,
}

pub type Errors = HashMap<String, Vec<String>>;

#[derive(Serialize, Deserialize, Debug, Clone, Copy, Default)]
pub struct Pagination {
    #[serde(skip_serializing_if = "Option::is_none")]
    total: Option<usize>,
    #[serde(skip_serializing_if = "Option::is_none")]
    per_page: Option<usize>,
    #[serde(skip_serializing_if = "Option::is_none")]
    current_page: Option<usize>,
    #[serde(skip_serializing_if = "Option::is_none")]
    last_page: Option<usize>,
    #[serde(skip_serializing_if = "Option::is_none")]
    from: Option<usize>,
    #[serde(skip_serializing_if = "Option::is_none")]
    to: Option<usize>,
}

impl Pagination {
    pub fn new(
        total: usize,
        per_page: usize,
        current_page: usize,
        last_page: usize,
        from: usize,
        to: usize,
    ) -> Self {
        Pagination {
            total: Some(total),
            per_page: Some(per_page),
            current_page: Some(current_page),
            last_page: Some(last_page),
            from: Some(from),
            to: Some(to),
        }
    }
}
