# cron-runner

A crate for scheduling and running cron jobs.
The scheduler and each job within it runs within separate threads
and is therefore non-blocking.

## Usage

```rust
lazy_static! {
    static ref jobs: [CronJob; 1] = [
        CronJob::new("0 0 * * * * *", Box::new(do_the_thing_hourly)).unwrap(),
    ];
} 

fn do_the_thing_hourly() -> Result<(), String> {
    // TODO: Things
    Ok(())
}

fn main() {
    cron_runner::start(&*CRON_JOBS);
    // Some blocking loop that keeps the application alive
}

```