#[macro_use]
extern crate log;
#[macro_use]
extern crate thiserror;

use anyhow::Result;
use chrono::Utc;
use cron::Schedule;
use std::ops::Fn;
use std::str::FromStr;
use std::time::Duration;

pub async fn start(jobs: &'static [CronJob]) {
    info!("Starting cron runner");
    loop {
        let now = Utc::now();
        for job in jobs {
            if job.schedule().upcoming(Utc).next().unwrap().timestamp() - 1 == now.timestamp() {
                actix_rt::spawn(job.run());
            }
        }
        tokio::time::delay_for(Duration::new(1, 0)).await;
    }
}

pub struct CronJob {
    schedule: Schedule,
    method: Box<dyn Fn() -> Result<()> + Sync>,
}

impl CronJob {
    /// Initialize a cron job, returning an error if the cron string is invalid
    #[allow(clippy::new_ret_no_self)]
    pub fn new(schedule: &str, method: Box<dyn Fn() -> Result<()> + Sync>) -> Result<Self> {
        let schedule = Schedule::from_str(schedule).map_err(|_| CronError::InvalidCronString {
            cron_string: schedule.into(),
        })?;
        Ok(CronJob { schedule, method })
    }

    /// Returns a cron string representing the intended schedule for this job
    pub fn schedule(&self) -> &Schedule {
        &self.schedule
    }

    /// Runs the job
    pub async fn run(&self) {
        let _ = (self.method)().map_err(|e| error!("Cron job failed: {}", e));
    }
}

#[derive(Debug, Clone, Error)]
pub enum CronError {
    #[error("invalid cron string: {cron_string}")]
    InvalidCronString { cron_string: String },
}
