use std::env;

use actix::Addr;
#[cfg(feature = "diesel")]
pub use actix_r2d2::{create_executors, DbExecutor, DbMessage, LoggingConnection};
use actix_service::ServiceFactory;
#[cfg(feature = "sqlx")]
pub use actix_sqlx::{create_executors, DbExecutor, DbMessage, PgPool};
use actix_web::dev::{MessageBody, ServiceRequest, ServiceResponse};
use actix_web::error::Error;
use actix_web::middleware::Logger;
use actix_web::{App, HttpServer};
use cron_runner;
pub use cron_runner::{CronError, CronJob};
use job_runner;
#[cfg(feature = "diesel")]
pub use job_runner::jobs;
pub use job_runner::{Job, JobStatus, JobType, NewJob};
pub use jwt_simple::{JwtAuth, JwtError, Token};
pub use response_wrapper::*;

pub fn setup_rust_log(app_name: &str) {
    let envs = env::var("RUST_LOG").unwrap_or_else(|_| format!("info,{}=debug", app_name));
    env::set_var("RUST_LOG", envs);
}

pub struct ActixStarter<F> {
    actix_builder: Option<F>,
    background_jobs: Option<&'static [JobType]>,
    cron_jobs: Option<&'static [CronJob]>,
    database_url: Option<String>,
}

#[allow(dead_code)]
pub struct BasicState {
    pub db: Addr<DbExecutor>,
}

/// Useful for implementing extractors and things like that
pub trait DbHandlingState {
    fn db(&self) -> &Addr<DbExecutor>;
}

impl DbHandlingState for BasicState {
    fn db(&self) -> &Addr<DbExecutor> {
        &self.db
    }
}

impl<F, T, B> ActixStarter<F>
where
    F: Fn() -> App<T, B> + Clone + Send + 'static,
    B: MessageBody + 'static,
    T: ServiceFactory<
            Config = (),
            Request = ServiceRequest,
            Response = ServiceResponse<B>,
            Error = Error,
            InitError = (),
        > + 'static,
{
    pub fn new() -> Self {
        ActixStarter {
            actix_builder: None,
            background_jobs: None,
            cron_jobs: None,
            database_url: None,
        }
    }

    pub fn set_actix_builder(mut self, actix_builder: F) -> Self {
        self.actix_builder = Some(actix_builder);
        self
    }

    pub fn set_background_jobs(mut self, background_jobs: &'static [JobType]) -> Self {
        self.background_jobs = Some(background_jobs);
        self
    }

    pub fn set_cron_jobs(mut self, cron_jobs: &'static [CronJob]) -> Self {
        self.cron_jobs = Some(cron_jobs);
        self
    }

    pub fn set_database_url(mut self, database_url: String) -> Self {
        self.database_url = Some(database_url);
        self
    }

    pub async fn start(self) -> std::io::Result<()> {
        if let Some(cron_jobs) = self.cron_jobs {
            actix_rt::spawn(cron_runner::start(cron_jobs));
        }
        if let Some(background_jobs) = self.background_jobs {
            actix_rt::spawn(job_runner::start(
                self.database_url
                    .expect("Database URL must be set for background job runner to function"),
                background_jobs,
            ));
        }
        let builder = self
            .actix_builder
            .expect("Actix builder method must be provided");
        HttpServer::new(move || (builder)().wrap(Logger::default()))
            .bind(format!(
                "{}:{}",
                env::var("SERVER_ADDR").unwrap_or_else(|_| "127.0.0.1".to_string()),
                env::var("SERVER_PORT").unwrap_or_else(|_| "8080".to_string())
            ))?
            .run()
            .await
    }
}

impl<F, T, B> Default for ActixStarter<F>
where
    F: Fn() -> App<T, B> + Clone + Send + 'static,
    B: MessageBody + 'static,
    T: ServiceFactory<
            Config = (),
            Request = ServiceRequest,
            Response = ServiceResponse<B>,
            Error = Error,
            InitError = (),
        > + 'static,
{
    fn default() -> Self {
        ActixStarter::new()
    }
}
