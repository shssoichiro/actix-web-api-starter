CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE TYPE job_status AS ENUM ('not_run', 'finished', 'failed');
CREATE TABLE jobs (
  id         UUID PRIMARY KEY     DEFAULT gen_random_uuid(),
  queue      VARCHAR(40) NOT NULL,
  payload    JSONB       NOT NULL,
  status     job_status  NOT NULL DEFAULT 'not_run',
  priority   SMALLINT    NOT NULL DEFAULT 255,
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX idx_jobs_status_created_at
  ON jobs (status, created_at);
SELECT diesel_manage_updated_at('jobs');
