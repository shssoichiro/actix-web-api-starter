#[cfg(feature = "diesel_use")]
#[macro_use]
extern crate diesel;
#[cfg(feature = "diesel_use")]
#[macro_use]
extern crate diesel_derive_enum;
#[cfg(feature = "diesel_use")]
#[macro_use]
extern crate diesel_derives_extra;
#[macro_use]
extern crate log;

use anyhow::Result;
#[cfg(feature = "diesel_use")]
use diesel_derives_traits;
#[cfg(feature = "diesel_use")]
use diesel_derives_traits::Model;
use serde_json::Value;
use std::time::Duration;

pub use crate::job::*;

pub mod job;

#[allow(unused_mut)]
#[allow(clippy::unnecessary_mut_passed)]
pub async fn start(db_url: String, job_types: &'static [JobType]) {
    #[cfg(feature = "diesel_use")]
    let mut conn = {
        use diesel::{Connection, ConnectionError};
        use diesel_logger::LoggingConnection;
        use retry::{delay, retry, OperationResult};

        match retry(
            delay::Fixed::from_millis(5000).take(10),
            || match LoggingConnection::establish(&db_url) {
                Ok(conn) => OperationResult::Ok(conn),
                Err(ConnectionError::InvalidCString(e)) => {
                    OperationResult::Err(ConnectionError::InvalidCString(e))
                }
                Err(ConnectionError::InvalidConnectionUrl(e)) => {
                    OperationResult::Err(ConnectionError::InvalidConnectionUrl(e))
                }
                Err(e) => OperationResult::Retry(e),
            },
        ) {
            Ok(conn) => conn,
            Err(e) => {
                error!("Failed to start job runner: {}", e);
                return;
            }
        }
    };

    #[cfg(feature = "sqlx_use")]
    let mut conn = {
        use sqlx::{Connect, PgConnection};

        let conn;
        let mut i = 1u8;
        loop {
            match PgConnection::connect(&db_url).await {
                Ok(inner_conn) => {
                    conn = inner_conn;
                    break;
                }
                Err(sqlx::Error::UrlParse(e)) => {
                    error!("Failed to start job runner: {}", e);
                    return;
                }
                Err(e) => {
                    if i >= 10 {
                        error!("Failed to start job runner: {}", e);
                        return;
                    } else {
                        i += 1;
                        tokio::time::delay_for(Duration::from_secs(5)).await;
                    }
                }
            };
        }
        conn
    };

    info!("Starting job runner");
    loop {
        let job = Job::find_next_to_run(&mut conn).await;
        match job {
            Ok(Some(mut job)) => {
                let result = job_types
                    .iter()
                    .find(|job_type| job_type.name == job.queue.as_str())
                    .ok_or_else(|| format!("Specified job not found: {}", job.queue))
                    .and_then(|job_type| {
                        (job_type.method)(job.payload.clone()).map_err(|e| e.to_string())
                    });
                #[cfg(feature = "diesel_use")]
                {
                    job.status = if let Err(e) = result {
                        warn!("Job {} failed: {}", job.id, e);
                        JobStatus::Failed
                    } else {
                        JobStatus::Finished
                    };
                    let _ = job
                        .save(&conn)
                        .map_err(|e| warn!("Failed to update job status: {}", e));
                }
                #[cfg(feature = "sqlx_use")]
                {
                    let _ = sqlx::query(
                        "UPDATE jobs \
                        SET status = $1 \
                        WHERE id = $2",
                    )
                    .bind(if let Err(e) = result {
                        warn!("Job {} failed: {}", job.id, e);
                        JobStatus::Failed
                    } else {
                        JobStatus::Finished
                    })
                    .bind(job.id)
                    .execute(&mut conn)
                    .await
                    .map_err(|e| warn!("Failed to update job status: {}", e));
                }
            }
            Ok(None) => tokio::time::delay_for(Duration::new(5, 0)).await,
            Err(e) => {
                error!("Failed to query for next job: {}", e);
                tokio::time::delay_for(Duration::new(15, 0)).await
            }
        }
    }
}

type JobMethod = Box<dyn Fn(Value) -> Result<()> + Sync>;

pub struct JobType {
    name: &'static str,
    method: JobMethod,
}

impl JobType {
    pub fn new(name: &'static str, method: JobMethod) -> Self {
        JobType { name, method }
    }
}
