#![allow(proc_macro_derive_resolution_fallback)]

use anyhow::Result;
use chrono::{DateTime, Utc};
#[cfg(feature = "diesel_use")]
use diesel::pg::PgConnection;
#[cfg(feature = "diesel_use")]
use diesel::prelude::*;
#[cfg(feature = "diesel_use")]
use diesel_logger::LoggingConnection;
use serde::{Deserialize, Serialize};
use serde_json::to_value;
use serde_json::Value;
#[cfg(feature = "sqlx_use")]
use sqlx::executor::RefExecutor;
#[cfg(feature = "sqlx_use")]
use sqlx::postgres::PgQueryAs;
#[cfg(feature = "sqlx_use")]
use sqlx::value::HasRawValue;
#[cfg(feature = "sqlx_use")]
use sqlx::{Database, Postgres};
use uuid::Uuid;

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "diesel_use", derive(DbEnum))]
pub enum JobStatus {
    NotRun,
    Finished,
    Failed,
}

#[cfg(feature = "sqlx_use")]
impl sqlx::Type<Postgres> for JobStatus {
    fn type_info() -> <Postgres as Database>::TypeInfo {
        String::type_info()
    }
}

#[cfg(feature = "sqlx_use")]
impl sqlx::encode::Encode<Postgres> for JobStatus {
    fn encode(&self, buf: &mut <Postgres as Database>::RawBuffer) {
        match *self {
            JobStatus::NotRun => "not_run",
            JobStatus::Finished => "finished",
            JobStatus::Failed => "failed",
        }
        .encode(buf)
    }
}

#[cfg(feature = "sqlx_use")]
impl sqlx::decode::Decode<'_, Postgres> for JobStatus {
    fn decode(
        value: <Postgres as HasRawValue>::RawValue,
    ) -> std::result::Result<Self, sqlx::Error> {
        let result: String = sqlx::decode::Decode::decode(value)?;
        Ok(match result.as_str() {
            "not_run" => JobStatus::NotRun,
            "finished" => JobStatus::Finished,
            "failed" => JobStatus::Failed,
            _ => JobStatus::Failed,
        })
    }
}

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "diesel_use",
    derive(Queryable, Identifiable, AsChangeset, Model)
)]
#[cfg_attr(feature = "diesel_use", table_name = "jobs")]
#[cfg_attr(feature = "diesel_use", changeset_options(treat_none_as_null = "true"))]
#[cfg_attr(feature = "sqlx_use", derive(sqlx::FromRow))]
pub struct Job {
    pub id: Uuid,
    pub queue: String,
    pub payload: Value,
    pub status: JobStatus,
    pub priority: i16,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

impl Job {
    #[cfg(feature = "diesel_use")]
    pub async fn find_next_to_run(conn: &LoggingConnection<PgConnection>) -> Result<Option<Self>> {
        jobs::dsl::jobs
            .filter(jobs::dsl::status.eq(JobStatus::NotRun))
            .order((jobs::dsl::priority.asc(), jobs::dsl::created_at.asc()))
            .get_result(conn)
            .optional()
            .map_err(|e| e.into())
    }

    #[cfg(feature = "sqlx_use")]
    pub async fn find_next_to_run<'e, E>(conn: E) -> Result<Option<Self>>
    where
        E: 'e + Send + RefExecutor<'e, Database = Postgres>,
    {
        sqlx::query_as(
            "SELECT id, queue, payload, status, priority, created_at, updated_at \
            FROM jobs \
            WHERE status = cast($1 AS job_status) \
            ORDER BY priority ASC, created_at ASC",
        )
        .bind(JobStatus::NotRun)
        .fetch_optional(conn)
        .await
        .map_err(|e| e.into())
    }
}

#[derive(Debug, Clone)]
#[cfg_attr(feature = "diesel_use", derive(Insertable, NewModel))]
#[cfg_attr(feature = "diesel_use", table_name = "jobs")]
#[cfg_attr(feature = "diesel_use", model(Job))]
pub struct NewJob {
    pub queue: &'static str,
    pub payload: Value,
    pub priority: i16,
}

impl NewJob {
    pub fn new<T: Serialize>(queue: &'static str, payload: T, priority: i16) -> Self {
        NewJob {
            queue,
            payload: to_value(payload).unwrap(),
            priority,
        }
    }

    #[cfg(feature = "sqlx_use")]
    pub async fn save<'e, E>(self, conn: E) -> Result<Job>
    where
        E: 'e + Send + RefExecutor<'e, Database = Postgres>,
    {
        sqlx::query_as(
            "INSERT INTO jobs (queue, payload, priority) \
            VALUES ($1, $2, $3) \
            RETURNING *",
        )
        .bind(self.queue)
        .bind(self.payload)
        .bind(self.priority)
        .fetch_one(conn)
        .await
        .map_err(|e| e.into())
    }
}

#[cfg(feature = "diesel_use")]
table! {
    use diesel::sql_types::*;
    use super::JobStatusMapping;
    jobs (id) {
        id -> Uuid,
        queue -> Varchar,
        payload -> Jsonb,
        status -> JobStatusMapping,
        priority -> SmallInt,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}
