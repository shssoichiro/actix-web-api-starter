# job-runner

A crate for queueing background jobs. Currently supports
a database backend. Hopes to support other backends in the future.

## Usage

```rust
use diesel::PgConnection;
use serde_json::{Value, from_value};
use job_runner::*;

pub const QUEUED_JOB_1: &str = "QUEUED_JOB_1";

lazy_static! {
    pub static ref JOB_TYPES: [JobType; 1] = [
        JobType::new(QUEUED_JOB_1, Box::new(do_the_queued_thing)),
    ];
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct QueuedJobPayload {
    datum: String,
    info: i32,
}

impl QueuedJobPayload {
    pub fn new(datum: String, info: i32) -> Self {
        QueuedJobPayload { datum, info }
    }
}

fn do_the_queued_thing(payload: Value) -> Result<(), String> {
    let payload: ActivationMailerPayload = from_value(payload).map_err(|e| e.to_string())?;
    // TODO: Take some action on the JSON payload
    Ok(())
}

fn enqueue_the_thing(conn: &PgConnection) {
    let payload = QueuedJobPayload::new("foo".to_string(), 123);
    NewJob::new(jobs::QUEUED_JOB_1, payload, 1)
        .save(conn)
        .unwrap();
}

fn main() {
    job_runner::start(
        &env::var("DATABASE_URL").expect("DATABASE_URL not set"),
        &*jobs::JOB_TYPES,
    ).unwrap();
    // Some blocking loop that keeps the application alive
}
```