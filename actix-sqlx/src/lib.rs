use actix;
use actix::prelude::*;
use anyhow::Result;

pub use sqlx::PgPool;

pub struct DbExecutor(pub PgPool);

impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}

pub trait DbMessage: Message {
    fn handle(self, pool: &PgPool) -> Self::Result;
}

impl<T> Handler<T> for DbExecutor
where
    T: DbMessage + Message,
    <T as Message>::Result: actix::dev::MessageResponse<DbExecutor, T>,
{
    type Result = <T as Message>::Result;

    fn handle(&mut self, msg: T, _: &mut Self::Context) -> Self::Result {
        msg.handle(&self.0)
    }
}

pub async fn create_executors(db_url: &str) -> Result<Addr<DbExecutor>> {
    let pool = PgPool::new(db_url).await?;
    Ok(SyncArbiter::start(3, move || DbExecutor(pool.clone())))
}
